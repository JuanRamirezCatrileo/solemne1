<%-- 
    Document   : index
    Created on : 06-05-2021, 00:36:18
    Author     : Juan Ramirez
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calculadora de Interes</title>
    </head>
    <body>
        <form  name="CONTROLADOR" action="controlador" method="POST">

            <h1><p align="center">Calculadora</p></h1>
            <p align="center">
    
            <label for="capital"> Ingrese el Capital:</label>
            <input type="text" id="capital" name="capital" placeholder="Ingrese su Capital ">
            
            <label for="tasa"> Ingrese la Tasa de interes anual:</label>
            <input type="text" id="tasa" name="tasa" placeholder="Tasa de Interes ">
            
            <label for="años"> Ingrese el numero de años:</label>
            <input type="text" id="anos" name="anos" placeholder="Ingrese los años ">
                   
            
            <button type="submit" class="btn btn-success">calcular</button>
            </p>
        </form>
    </body>
</html>

/*
    Document   : resultado
    Created on : 06-05-2021, 00:36:27
    Author     : Juan Ramirez
*/


package cl.modelo;

public class modelo
{
    private float cap;
    private float tas;
    private float ano;
        
public modelo (float cap, float tas, float ano)
{
    this.cap = cap;
    this.tas= tas;
    this.ano= ano;
}
 
    /**
     *
     * @return
     */
    public float calculadorainteres() 
{
   float interes = (this.cap* (this.tas/100))*this.ano;
   return interes;   
}
 
public float getcapital(){
  return cap;
}

public void setcap(float cap){
    this.cap = cap;
    
}

public float getctas(){
  return tas;
}

public void setctas(float tas){
    this.tas = tas;
    
}
public float getaño(){
  return ano;
}

    /**
     *
     * @param ano
     */
    public void setaño(float ano){
    this.ano = ano;
    
}


}
